terraform {
  backend "s3" {
    bucket  = "mybucket1nov11"
    key     = "dev/terraform.tfstate"
    region  = "ap-south-1"
    encrypt = true
    dynamodb_table = "test1"

  }
}
